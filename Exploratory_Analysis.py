# %%
import pandas as pd
import numpy as np

# %%
df = pd.read_csv("Enter path of csv file containing hate-speech dataset")
#remove columns 'Unnamed: 0', 'acl.id', 'X1', 'split', 'round.base', 'annotator', 'round', 'acl.id.matched'
df = df.drop(['Unnamed: 0', 'acl.id', 'X1', 'split', 'round.base', 'annotator', 'round', 'acl.id.matched'], axis=1)

# %%
df.head()

# %%
#perform basic preprocessing of text data in text column using NLP techniques
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('punkt')
stop_words = set(stopwords.words('english'))
lemmatizer = WordNetLemmatizer()
def clean_text(text):
    text = text.lower() #convert to lowercase
    text = re.sub(r'[^\w\s]', '', text) #remove punctuation
    text = re.sub(r'\$\$', ' ', text)  # Example: replacing '$$' with a space
    # Add more substitutions here based on your dataset's needs
    text = re.sub(r'[^a-zA-Z0-9\s]', ' ', text)
    words = word_tokenize(text) #tokenize
    words = [w for w in words if w not in stop_words] #remove stopwords
    words = [lemmatizer.lemmatize(w) for w in words] #lemmatize
    text = ' '.join(words)
    return text
df['text_pre_processed_nltk'] = df['text'].apply(clean_text)


# %%
#perform basic preprocessing of text data in text column using NLP techniques using spacy

import spacy
nlp = spacy.load('en_core_web_sm')
import re

def clean_text_spacy(docs):
    for doc in docs:
        text = re.sub(r'\$\$', ' ', doc.text)  # Example: replacing '$$' with a space
        # Add more substitutions here based on your dataset's needs
        text = re.sub(r'[^a-zA-Z0-9\s]', ' ', doc.text)
        text = re.sub(r'[^\w\s]', '', doc.text.lower()) # convert to lowercase and remove punctuation
        words = [token.lemma_ for token in doc if not token.is_stop] # remove stopwords and lemmatize
        yield ' '.join(words)

# Apply the function to the 'text' column
df['text_pre_processed'] = list(clean_text_spacy(nlp.pipe(df['text'], n_process=20,batch_size=1000)))

# %%


# %%
import pandas as pd
from transformers import BertTokenizer, BertModel
import torch
from sklearn.manifold import TSNE
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt




# Filter for specific targets
targets_of_interest = ['wom', 'bla', 'jew', 'mus', 'trans', 'gay', 'immig', 'dis', 'ref', 'arab']
filtered_df = df[df['target'].isin(targets_of_interest)]

# Initialize BERT tokenizer and model
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model = BertModel.from_pretrained('bert-base-uncased')

# Tokenize and encode sentences for BERT
tokens = {'input_ids': [], 'attention_mask': []}

for text in filtered_df['text_pre_processed']:
    new_tokens = tokenizer.encode_plus(text, max_length=128, truncation=True, padding='max_length', return_tensors='pt')
    tokens['input_ids'].append(new_tokens['input_ids'][0])
    tokens['attention_mask'].append(new_tokens['attention_mask'][0])

tokens['input_ids'] = torch.stack(tokens['input_ids'])
tokens['attention_mask'] = torch.stack(tokens['attention_mask'])

# Generate embeddings
with torch.no_grad():
    outputs = model(tokens['input_ids'], attention_mask=tokens['attention_mask'])
    embeddings = outputs.last_hidden_state[:,0,:].numpy()  # Take the embeddings from the first token ([CLS])

# Dimensionality reduction with t-SNE
tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
reduced_embeddings = tsne.fit_transform(embeddings)

# Clustering with KMeans
kmeans = KMeans(n_clusters=len(targets_of_interest))  # Number of clusters equals the number of targets
clusters = kmeans.fit_predict(reduced_embeddings)

# Visualization
plt.figure(figsize=(12, 8))
scatter = plt.scatter(reduced_embeddings[:, 0], reduced_embeddings[:, 1], c=clusters, cmap='viridis')
plt.legend(handles=scatter.legend_elements()[0], labels=targets_of_interest)
plt.title('t-SNE of BERT Embeddings for Targeted Hate Speech')
plt.xlabel('t-SNE dimension 1')
plt.ylabel('t-SNE dimension 2')
plt.show()

# %%
# import pandas as pd
# import numpy as np
# from gensim.models import Word2Vec
# from sklearn.decomposition import PCA
# from sklearn.cluster import KMeans
# import matplotlib.pyplot as plt
# from sklearn.manifold import TSNE

# # Preprocess the text data
# # Here, you would tokenize the sentences in your 'text' column and possibly clean the text
# # For simplicity, this example assumes the 'text' column contains the sentences you want to analyze
# # You might need to adjust this preprocessing based on your dataset's specifics

# # Tokenize sentences into words
# df['tokenized_text'] = df['text_pre_processed'].apply(lambda x: x.split())  # Simple split; consider using more sophisticated tokenization

# # Collect all tokenized sentences
# documents = df['tokenized_text'].tolist()

# # Train a Word2Vec model
# model = Word2Vec(sentences=documents, vector_size=100, window=5, min_count=1, workers=4)

# # Generate embeddings for all unique words in the dataset
# words = list(model.wv.index_to_key)  # This gets the unique words in the model's vocabulary
# word_vectors = np.array([model.wv[word] for word in words])

# # Dimensionality reduction using PCA or t-SNE
# # Here, we'll stick with PCA as shown, but t-SNE is also a good option for visualization
# pca = PCA(n_components=2)
# reduced_vectors = pca.fit_transform(word_vectors)

# # Clustering using KMeans
# kmeans = KMeans(n_clusters=3)  # The number of clusters might need adjustment based on your data
# clusters = kmeans.fit_predict(reduced_vectors)

# # Plotting
# plt.scatter(reduced_vectors[:, 0], reduced_vectors[:, 1], c=clusters)
# for i, word in enumerate(words):
#     plt.annotate(word, xy=(reduced_vectors[i, 0], reduced_vectors[i, 1]))
# plt.show()


# %%
import pandas as pd
import numpy as np
from gensim.models import Word2Vec
from sklearn.cluster import MiniBatchKMeans
from sklearn.decomposition import IncrementalPCA
import matplotlib.pyplot as plt
from nltk.tokenize import word_tokenize

# Assuming 'df' is a pandas DataFrame with a column 'text_pre_processed' containing pre-processed text

# Efficient tokenization using nltk
df['tokenized_text'] = df['text_pre_processed'].apply(word_tokenize)

# Collect all tokenized sentences
documents = df['tokenized_text'].tolist()

# Train a Word2Vec model with a smaller vector size and using all available cores
model = Word2Vec(sentences=documents, vector_size=50, window=5, min_count=1, workers=4)

# Generate embeddings for all unique words in the dataset
words = list(model.wv.index_to_key)
word_vectors = np.array([model.wv[word] for word in words])

# Dimensionality reduction using IncrementalPCA
pca = IncrementalPCA(n_components=2)
reduced_vectors = pca.fit_transform(word_vectors)

# Clustering using MiniBatchKMeans
kmeans = MiniBatchKMeans(n_clusters=3)
clusters = kmeans.fit_predict(reduced_vectors)

# Plotting
plt.scatter(reduced_vectors[:, 0], reduced_vectors[:, 1], c=clusters)
for i, word in enumerate(words):
    plt.annotate(word, xy=(reduced_vectors[i, 0], reduced_vectors[i, 1]), fontsize=8)
plt.show()


# %%
# # Plotting
# plt.scatter(reduced_vectors[:, 0], reduced_vectors[:, 1], c=clusters)
# for i, word in enumerate(words):
#     plt.annotate(word, xy=(reduced_vectors[i, 0], reduced_vectors[i, 1]), fontsize=8)
# plt.show()

#increase size of the above plot and add legend to the graph
plt.figure(figsize=(12, 8))
scatter = plt.scatter(reduced_vectors[:, 0], reduced_vectors[:, 1], c=clusters, cmap='viridis')
plt.legend(handles=scatter.legend_elements()[0], labels=['Cluster 1', 'Cluster 2', 'Cluster 3'])
plt.title('Word Embeddings with Clusters')
plt.xlabel('PCA dimension 1')
plt.ylabel('PCA dimension 2')
plt.show()


