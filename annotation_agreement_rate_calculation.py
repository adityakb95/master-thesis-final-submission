# %%
import pandas as pd
import krippendorff


df = pd.read_csv(r"Enter the path of the file here containing all annotations")

# %%
#perform the above using for loop with columns in list
columns = ['bertagent', 'human', 'Riveter', 'Adi', 'Claudia', 'Zehui', 'Leon', 'Dennis', 'classification1_agency']
for column in columns:
    df[column] = df[column].replace(['high', 'low', 'neutral'], ['High', 'Low', 'Neutral'])



#Below is code for correlation matrix creation incase it is needed(commented out)
# # %%
# for i in columns:
#     df[i] = df[i].astype('category')
#     df[i] = df[i].cat.codes

# #now calculate the correlation matrix for the above columns per iteration column value
# for i in df.iteration.unique():
#     corr = df[df['iteration']==i][['bertagent', 'human', 'Riveter', 'Adi', 'Claudia', 'Zehui', 'Leon', 'Dennis']].corr()
#     corr.to_csv(r"C:\Users\adity\OneDrive - Students RWTH Aachen University\Master Thesis\Dehumanization\Correlation\CorrelationMatrix_"+str(i)+".csv")
# corr = df[['bertagent', 'human', 'Riveter', 'Adi', 'Claudia', 'Zehui', 'Leon', 'Dennis']].corr()

# %%
#Below is code for kappa constant calculation incase it is needed(commented out)
# df = df.astype(str)

# # %%
# df1 = df.copy(deep=True)

# # %%
# df1 = df1.head(60)

# # %%
# df = df1.copy(deep=True)
# df = df.head(60)
# df = df.loc[df['iteration'] == '4']

# # %%
# from sklearn.metrics import cohen_kappa_score
# import numpy as np

# #cols = ['bertagent', 'human', 'Riveter', 'Adi', 'Claudia', 'Zehui', 'Leon', 'Dennis', 'classification1_agency']
# cols = ['bertagent', 'human', 'Adi',  'Dennis','Zehui', 'classification1_agency']
# labels = ['High', 'Neutral', 'Not Sure', 'Low']
# n = len(cols)

# kappa_matrix = np.zeros((n, n))
# for i in range(n):
#     for j in range(n):
#         kappa_matrix[i, j] = cohen_kappa_score(df[cols[i]], df[cols[j]], labels=labels)

# kappa_df = pd.DataFrame(kappa_matrix, index=cols, columns=cols)

# %%



#caclculate the accuracy metrics such as accuracy, precision, recall, F1 score for the columns 'bertagent', 'human','classification1_agency_baseline','classification1_agency_gpt3.5', 'classification1_agency_llama3' with 'human' as the ground truth

from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

cols = ['bertagent', 'human','classification1_agency_baseline','classification1_agency_gpt3.5', 'classification1_agency_llama3', 'classification1_agency_llama3_8b']

accuracy = []
precision = []
recall = []
f1 = []

for i in cols:
    accuracy.append(accuracy_score(df['human'], df[i]))
    precision.append(precision_score(df['human'], df[i], average='weighted',  zero_division=0))
    recall.append(recall_score(df['human'], df[i], average='weighted'))
    f1.append(f1_score(df['human'], df[i], average='weighted'))
    
metrics = pd.DataFrame({'accuracy': accuracy, 'precision': precision, 'recall': recall, 'f1': f1}, index=cols)
metrics

# %%
from sklearn.metrics import accuracy_score, f1_score
import pandas as pd

columns = ['bertagent', 'human', 'classification1_agency_baseline', 'classification1_agency_gpt3.5', 'classification1_agency_llama3', 'classification1_agency_llama3_8b']

# Initialize a list to store the results
results = []

# Loop over each unique iteration
for iteration in df['iteration'].unique():
    df_iteration = df[df['iteration'] == iteration]
    y_true = df_iteration['human']
    
    # Loop over each column
    for column in columns:
        y_pred = df_iteration[column]
        
        # Calculate accuracy and F1 score
        accuracy = accuracy_score(y_true, y_pred)
        f1 = f1_score(y_true, y_pred, average='weighted')
        
        # Append the results to the list
        results.append([iteration, column, accuracy, f1])

# Convert the results to a DataFrame
results_df = pd.DataFrame(results, columns=['Iteration', 'Column', 'Accuracy', 'F1 Score'])

# Display the DataFrame
results_df.sort_values(by=['Column','Iteration'], ascending=True)

# %%
label_mapping = {'High': 2, 'Neutral': 1, 'Low': 0}

df['bertagent_num'] = df['bertagent'].map(label_mapping)
df['human_num'] = df['human'].map(label_mapping)
df['classification1_agency_baseline_num'] = df['classification1_agency_baseline'].map(label_mapping)
df['classification1_agency_gpt3.5_num'] = df['classification1_agency_gpt3.5'].map(label_mapping)
df['classification1_agency_llama3_num'] = df['classification1_agency_llama3'].map(label_mapping)
df['classification1_agency_llama3_8b_num'] = df['classification1_agency_llama3_8b'].map(label_mapping)

# %%
from sklearn.metrics import log_loss

# Convert labels to one-hot encoding
y_true = pd.get_dummies(df['human_num'])

# List of columns to calculate cross-entropy loss for
columns = ['bertagent_num', 'classification1_agency_baseline_num', 'classification1_agency_gpt3.5_num', 'classification1_agency_llama3_num', 'classification1_agency_llama3_8b_num']

# Loop over each column
for column in columns:
    # Convert labels to one-hot encoding
    y_pred_proba = pd.get_dummies(df[column])
    
    # Ensure y_pred_proba has the same columns as y_true
    y_pred_proba = y_pred_proba.reindex(columns = y_true.columns, fill_value=0)
    
    # Calculate cross-entropy loss
    ce_loss = log_loss(y_true.values, y_pred_proba.values)
    
    # Print the cross-entropy loss
    print(f"Cross-entropy loss({column}): {ce_loss:.3f}")

# %%


# %%
from sklearn.metrics import log_loss

columns = ['bertagent_num', 'classification1_agency_baseline_num', 'classification1_agency_gpt3.5_num', 'classification1_agency_llama3_num', 'classification1_agency_llama3_8b_num']

# Initialize a list to store the results
results = []

# Loop over each unique iteration
for iteration in df['iteration'].unique():
    df_iteration = df[df['iteration'] == iteration]
    
    # Convert labels to one-hot encoding
    y_true = pd.get_dummies(df_iteration['human_num'])

    # Loop over each column
    for column in columns:
        # Convert labels to one-hot encoding
        y_pred_proba = pd.get_dummies(df_iteration[column])
        y_pred_proba = y_pred_proba.reindex(columns = y_true.columns, fill_value=0)
        # Calculate cross-entropy loss
        # try: 
        #     ce_loss = log_loss(y_true, y_pred_proba)
        # except ValueError:
        #     ce_loss = -1
        ce_loss = log_loss(y_true.values, y_pred_proba.values)
        # Append the results to the list
        results.append([iteration, column, ce_loss])

# Convert the results to a DataFrame
results_df = pd.DataFrame(results, columns=['Iteration', 'Column', 'Cross-Entropy Loss'])

# Display the DataFrame
results_df.sort_values(by=['Column','Iteration'], ascending=True)

# %%
from scipy.spatial.distance import jensenshannon

columns = ['bertagent_num', 'human_num', 'classification1_agency_baseline_num', 'classification1_agency_gpt3.5_num', 'classification1_agency_llama3_num', 'classification1_agency_llama3_8b_num']

# Initialize a list to store the results
results = []

# Calculate probability distribution for 'human_num'
p = df['human_num'].value_counts(normalize=True)

# Loop over each column
for column in columns:
    # Calculate probability distribution
    q = df[column].value_counts(normalize=True)
    
    # Ensure distributions have same support
    for label in p.index.union(q.index):
        if label not in p.index:
            p[label] = 0
        if label not in q.index:  
            q[label] = 0

    p = p.sort_index()
    q = q.sort_index()

    # Calculate Jensen-Shannon divergence 
    js_div = jensenshannon(p, q)
    
    # Append the results to the list
    results.append([column, js_div])

# Convert the results to a DataFrame
results_df = pd.DataFrame(results, columns=['Column', 'Jensen-Shannon Divergence'])

# Display the DataFrame
results_df

# %%
from scipy.spatial.distance import jensenshannon

columns = ['bertagent_num', 'human_num', 'classification1_agency_baseline_num', 'classification1_agency_gpt3.5_num', 'classification1_agency_llama3_num', 'classification1_agency_llama3_8b_num']

# Initialize a list to store the results
results = []

# Loop over each unique iteration
for iteration in df['iteration'].unique():
    df_iteration = df[df['iteration'] == iteration]
    
    # Calculate probability distribution for 'human_num'
    p = df_iteration['human_num'].value_counts(normalize=True)

    # Loop over each column
    for column in columns:
        # Calculate probability distribution
        q = df_iteration[column].value_counts(normalize=True)
        
        # Ensure distributions have same support
        for label in p.index.union(q.index):
            if label not in p.index:
                p[label] = 0
            if label not in q.index:  
                q[label] = 0

        p = p.sort_index()
        q = q.sort_index()

        # Calculate Jensen-Shannon divergence 
        js_div = jensenshannon(p, q)
        
        # Append the results to the list
        results.append([iteration, column, js_div])

# Convert the results to a DataFrame
results_df = pd.DataFrame(results, columns=['Iteration', 'Column', 'Jensen-Shannon Divergence'])

# Display the DataFrame
results_df.sort_values(by=['Column','Iteration'], ascending=True)

# %%
df[['bertagent', 'human','classification1_agency_gpt3.5', 'classification1_agency_llama3']].apply(pd.Series.value_counts)

# %%
import matplotlib.pyplot as plt
import pandas as pd

# Assuming data is already loaded into the DataFrame named 'data'
category_counts = df[['bertagent', 'human','classification1_agency_gpt3.5', 'classification1_agency_llama3']].apply(pd.Series.value_counts)

# Define a custom color for the bars
bar_colors = ['#2a9d8f', '#65c9a4', '#a4e5d3']  # A shade of teal green

# Plotting
fig, axs = plt.subplots(2, 2, figsize=(8, 8), sharey=True)  # Adjust this for more subplots
axs = axs.flatten()

for ax, (column_name, counts) in zip(axs, category_counts.items()):
    counts.plot(kind='bar', ax=ax, color=bar_colors, width=0.6)  # Increase the bar width
    ax.set_title(f'{column_name} Distribution')
    ax.set_xlabel('Agency Level')
    ax.set_ylabel('Frequency')
    ax.set_ylim(0, 40)  # Set consistent y-axis limit

plt.tight_layout()
plt.show()


# %%
import matplotlib.pyplot as plt
import pandas as pd

# Assuming data is already loaded into the DataFrame named 'data'
category_counts = df[['bertagent', 'human','classification1_agency_gpt3.5', 'classification1_agency_llama3']].apply(pd.Series.value_counts)

# Define custom colors for each category
colors = {
    'Low': '#a4e5d3',  # Lightest green
    'Neutral': '#65c9a4',  # Medium green
    'High': '#2a9d8f'  # Darkest green
}

# Plotting
fig, axs = plt.subplots(2, 2, figsize=(12, 10), sharey=True)  # Adjust the subplot layout
axs = axs.flatten()

for ax, (column_name, counts) in zip(axs, category_counts.items()):
    counts = counts.reindex(['Low', 'Neutral', 'High'])  # Ensure consistent order
    ax.bar(counts.index, counts, color=[colors[x] for x in counts.index], width=0.6)
    ax.set_title(f'{column_name.split("_")[-1].title()} Agency Level Distribution')
    ax.set_xlabel('Agency Level')
    ax.set_ylabel('Frequency')
    ax.set_ylim(0, 50)  # Adjust according to your data's range

plt.tight_layout()
plt.show()

# Below is code for Krippendorff's Alpha calculation
df1 = df.head(60).copy(deep=True)
df1 = df1.loc[df1['iteration'] == '4']
df1 = df1[['Adi', 'Claudia', 'Zehui', 'Leon','Dennis']]
#df1 = df1[['Adi', 'Claudia', 'Leon','Dennis']]

# %%
df1.iteration.value_counts()

# %%
df1 = df.copy(deep=True)
conversion_dict = {"Low": 0, "Neutral": 1, "High": 2, "Not Sure": np.nan}
for column in ['Adi', 'Claudia', 'Zehui', 'Leon', 'Dennis']:
#for column in ['Adi', 'Claudia', 'Leon', 'Dennis']:
    df1[column] = df1[column].map(conversion_dict)

reliability_data = df1[['Adi', 'Claudia', 'Zehui', 'Leon', 'Dennis']].T.values.tolist()
#reliability_data = df1[['Adi', 'Claudia', 'Leon', 'Dennis']].T.values.tolist()

# Calculate Krippendorff's Alpha for ordinal data
try:
    alpha = krippendorff.alpha(reliability_data=reliability_data, level_of_measurement='ordinal')
    print(f"Krippendorff's Alpha: {alpha}")
except Exception as e:
    print("Error calculating Krippendorff's Alpha:", e)

# %%
df1 = df.copy(deep=True)
conversion_dict = {"Low": 0, "Neutral": 1, "High": 2, "Not Sure": np.nan}
for column in ['Adi', 'Claudia', 'Leon', 'Dennis']:
    df1[column] = df1[column].map(conversion_dict)

# Loop over each unique iteration
for iteration in df1['iteration'].unique():
    df_iteration = df1[df1['iteration'] == iteration]
    
    reliability_data = df_iteration[['Adi', 'Claudia', 'Leon', 'Dennis']].T.values.tolist()

    # Calculate Krippendorff's Alpha for ordinal data
    try:
        alpha = krippendorff.alpha(reliability_data=reliability_data, level_of_measurement='ordinal')
        print(f"Iteration {iteration} - Krippendorff's Alpha: {alpha}")
    except Exception as e:
        print(f"Error calculating Krippendorff's Alpha for iteration {iteration}:", e)


