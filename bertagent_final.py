# %%
import pandas as pd


df1 = pd.read_csv("Enter path of csv file containing hate-speech dataset")

# Install BertAgent
# !pip install datasets
# !pip install bertagent
# !pip install transformers
# !pip install torch

# %%
#perform basic preprocessing of text data in text column using NLP techniques using spacy

import spacy
nlp = spacy.load('en_core_web_sm')
import re

def clean_text_spacy(docs):
    for doc in docs:
        text = re.sub(r'\$\$', ' ', doc.text)  # Example: replacing '$$' with a space
        # Add more substitutions here based on your dataset's needs
        text = re.sub(r'[^a-zA-Z0-9\s]', ' ', doc.text)
        text = re.sub(r'[^\w\s]', '', doc.text.lower()) # convert to lowercase and remove punctuation
        words = [token.lemma_ for token in doc if not token.is_stop] # remove stopwords and lemmatize
        yield ' '.join(words)

# Apply the function to the 'text' column
df1['text_pre_processed'] = list(clean_text_spacy(nlp.pipe(df1['text'], n_process=20,batch_size=1000)))

# %%
from bertagent import BERTAgent
from tqdm.notebook import tqdm
tqdm.pandas()

def bertagent(df,column_name):
    df["sents"] = df[column_name].str.split(".")
    ba0 = BERTAgent(tokenizer_params={'add_special_tokens': True, 'max_length': 512, 'padding': 'max_length', 'return_attention_mask': True, 'truncation': True})
    model_id = "ba0"
    
    df[model_id] = df.sents.progress_apply(ba0.predict)
    df["BATot"] = df[model_id].apply(ba0.tot)
    df["BAPos"] = df[model_id].apply(ba0.pos)
    df["BANeg"] = df[model_id].apply(ba0.neg)
    df["BAAbs"] = df[model_id].apply(ba0.abs)
    return df

# %%
from bertagent import BERTAgent
from tqdm.notebook import tqdm
tqdm.pandas()

def bertagent_pre_processed(df,column_name):
    df["sents"] = df[column_name].str.split(".")
    ba0 = BERTAgent(tokenizer_params={'add_special_tokens': True, 'max_length': 512, 'padding': 'max_length', 'return_attention_mask': True, 'truncation': True})
    model_id = "ba0"
    df[model_id] = df.sents.progress_apply(ba0.predict)
    df["BATot_pre_processed"] = df[model_id].apply(ba0.tot)
    df["BAPos_pre_processed"] = df[model_id].apply(ba0.pos)
    df["BANeg_pre_processed"] = df[model_id].apply(ba0.neg)
    df["BAAbs_pre_processed"] = df[model_id].apply(ba0.abs)
    return df

# %%
df1 = bertagent(df1,'text')
df1 = bertagent_pre_processed(df1,'text_pre_processed')


