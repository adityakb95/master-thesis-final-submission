# %%
import pandas as pd
import json
import groq

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
pd.set_option('display.width', None)

# %%
import os

from groq import Groq

client = Groq(
    api_key="enter your api key here",
)

# %%
file_path = r"enter path to your file here"

df = pd.read_excel(file_path, sheet_name = "Sheet1")

# %%
df1 = df.head(60)

# %%
prompt = ['''Given a text snippet, please analyze the description of actions, behaviors, and attributes related to the subject 'woman/women'. Classify the agency as 'High', 'Neutral', or 'Low'. 'High' should be used when the subject takes clear independent actions or makes decisions. 'Low' is for descriptions that present the subject as passive or portrayed as passive recipients of actions. Use 'Neutral' for descriptions that do not clearly fit either 'High' or 'Low', or when actions and passivity are equally presented. Provide a brief 1-2 sentence justification for your rating referencing specific words or phrases that influenced the agency level.

Examples of how to annotate agentic language:

<< Example 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
Step 1: Identify the phrase and verbs related to woman/women. Here they are “stupid ugly looking woman” and “all women end up being dead ugly”. 
Step 2: Identify the agency level with reasoning of all phrases related to woman/women. Both the phrases comment on womens’ look and does not comment on any particular level of autonomy or action on their part. Thus both give the agency level as “Neutral” and “Neutral”.
Step 3: Calculate the overall agency level based if it is all “High” then “High”, if it is all “Low” then “Low”, if there is any one phrase indicating “Neutral” agency then overall agency level will be “Neutral”. If there are both “High” and “Low” then again it will be “Neutral”. Here all are “Neutral”.
Step 4: Give explanation for the reasoning. 
Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
Step 5: Overall Agency: Neutral >>

<< Example 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
Step 1: Identify the phrase and verbs related to woman/women. Here they are “you were really weak in writing at school” ,”how have you become a writer ”and “women shouldnt be writers”. The first two indirectly refer to woman/women inferring from the third phrase where the speaker attributes the gender of the subject. 
Step 2: Identify the agency level with reasoning of all phrases related to woman/women. All three phrases here attribute passive recipients of actions to women, with the speaker showing pro-activeness in making the decision and not the woman herself. Thus all three phrases show “Low” agency.
Step 3: Calculate the overall agency level based if it is all “High” then “High”, if it is all “Low” then “Low”, if there is any one phrase indicating “Neutral” agency then overall agency level will be “Neutral”. If there are both “High” and “Low” then again it will be “Neutral”. Here all are “Low”
Step 4: Give explanation for the reasoning. 
Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
Step 5: Overall Agency: Low >>

<< Example 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
Step 1: Identify the phrase and verbs related to woman/women. Here they are “Julie was smoking”, “which women shouldn't do” and “dirty fucking tramp”. 
Step 2: Identify the agency level with reasoning of all phrases related to woman/women. The first phrase shows the woman i.e. Julie actively participating in an event i.e. smoking thus agency is “High”. The second phrase shows the speaker making the decision what woman should do making woman a passive recipients of actions. The third phrase is a comment on Julie’s appearance making it “Neutral” agency from the point of view of woman. Thus in all they give the agency level as “High” and “Low” and “Neutral”.
Step 3: Calculate the overall agency level based if it is all “High” then “High”, if it is all “Low” then “Low”, if there is any one phrase indicating “Neutral” agency then overall agency level will be “Neutral”. If there are both “High” and “Low” then again it will be “Neutral”. Overall agency level here is “Neutral”.
Step 4: Give explanation for the reasoning. 
Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
Step 5: Overall Agency: Neutral >>

<< Example 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
Step 1: Identify the phrase and verbs related to woman/women. Here they are “They expect from us, men” and “while they do nothing at all”. Both indirectly refer to woman/women inferring from the speaker attributing himself as a man and talking about the other gender. 
Step 2: Identify the agency level with reasoning of all phrases related to woman/women. Both phrases show women proactiveness and the power to make their own decision from the point of view of the speaker. Hence, the agency level is “High” and “High”
Step 3: Calculate the overall agency level based if it is all “High” then “High”, if it is all “Low” then “Low”, if there is any one phrase indicating “Neutral” agency then overall agency level will be “Neutral”. If there are both “High” and “Low” then again it will be “Neutral”. Overall agency will become “High”.
Step 4: Give explanation for the reasoning. 
Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them.
Step 5: Overall Agency: High >>
''']
column = ["classification1"]

# %%
# Ensure the 'text' column exists in your Excel sheet
if 'text' not in df1.columns:
    raise ValueError("The Excel file does not contain a 'text' column.")

#prompt = "Follow a chain of thought for determining the agency of 'women' within the text by breaking down the narrative into actions, behaviors, and descriptors. Then categorize the agency as 'High' where the text endows women with self-governance, 'Neutral' for ambiguous or undetermined agency, and 'Low' for depictions of dependency or lack of action."
for i in range(1):
    # A function that takes a piece of text and returns its classification
    def classify_text(text, human):
        # Construct the prompt with the definition, examples, and the text to classify
        system_prompt = f"{prompt[i]}"
        user_prompt = f"Please classify the following text according to the level of agency related to the subject 'woman/women':{text} using the 5 step process. Also, please end your response with overall agency level in a new line."
        # Make the API call to OpenAI using the chat endpoint
        response = client.chat.completions.create(
            model="llama3-8b-8192",  # Specify the model
            #model="llama3-70b-8192"
            messages=[{"role": "system", "content": system_prompt}, {"role": "user", "content": user_prompt}],  # System message with the prompt
            #messages=[ {"role": "user", "content": system_prompt + "\n" + user_prompt}],  # System message with the prompt
            
            temperature= 0.0,  # Set to 0 for more deterministic completions
            max_tokens=1024,  # Maximum number of tokens to generate; adjust as needed
            stop=None,  # Sequence where the API should stop generating further tokens
            n=1,  # The number of completions to generate, set to 1 for a single classification
            logprobs=None,  # Include log probabilities; set to None if not needed

        )
        # Extract the text from the response, ensuring we only get the classification part
        classification = response.choices[0].message.content
        return classification.strip("\n")

    # Apply the classification function to each row in the dataframe
    # The result is a new dataframe column with classifications
    #df1.loc[:,column[i]] = df1['text'].apply(classify_text)
    df1.loc[:,column[i]] = df1.apply(lambda x: classify_text(x.text, x.human), axis=1)

# Save the updated dataframe with classifications back to an Excel file
# Replace '/path/to/your/updated_Annotations.xlsx' with your desired output file path
# df.to_excel('/path/to/your/updated_Annotations.xlsx', index=False)

# print("Classification complete. The updated Excel file has been saved.")


# %%
# #show the full row and column
# pd.set_option('display.max_columns', None)
# pd.set_option('display.max_rows', None)
# pd.set_option('display.width', None)
# pd.set_option('display.max_colwidth', None)
df1.columns

# %%
#iterate all columns "classification1", "classification2", "classification3", "classification4", "classification5", "classification6" and extract the agency level by checking for the presence of "High", "Low" or "Neutral" in the column and put them in new column "agency_level"
column_agency = ["classification1_agency", "classification2_agency", "classification3_agency", "classification4_agency", "classification5_agency", "classification6_agency"]

for i in range(1):
    df1.loc[:,column_agency[i]] = df1[column[i]].apply(lambda x: 'High' if 'High' in x else ('Low' if 'Low' in x else 'Neutral'))



# %%
df1['classification1_agency'] = df1['classification1'].str.split("Overall Agency: ").str[1]

# %%
#write the above in a for loop for all 7 columns
for i in range(1):
    df1.loc[:, f'accuracy{i+1}'] = df1.apply(lambda x: 1 if x[column_agency[i]] == x['human'] else 0, axis = 1)

# %%
#print the sum of all accuracies
for i in range(1):
    print(df1[f'accuracy{i+1}'].sum())

# %%
df1.human.value_counts()

# %%
#print value counts of all columns classification_agency

for i in range(1):
    print(df1[column_agency[i]].value_counts())