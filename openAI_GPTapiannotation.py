# %%
import pandas as pd
import json
import openai

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
pd.set_option('display.width', None)

# %%
file_path = r"Enter the file path here"

df = pd.read_excel(file_path, sheet_name = "Sheet1")

# %%
df1 = df.head(60)


#Below are prompts that were tried for the task but didn't work well. They have been commented for convenience.
# %%
# prompt = [
# '''Given a text snippet, please analyze the description of actions, behaviors, and attributes related to the subject 'woman/women' and classify the agency as 'High', 'Neutral', or 'Low' based on the implied capacity for self-determination and independent action. For instance, attributing decision-making or action-taking roles to the subject would denote 'High' agency, while descriptions that present the subject without such roles or as passive should be annotated as 'Low'.''', 
# "Follow a chain of thought for determining the agency of 'women' within the text by breaking down the narrative into actions, behaviors, and descriptors. Then categorize the agency as 'High' where the text endows women with self-governance, 'Neutral' for ambiguous or undetermined agency, and 'Low' for depictions of dependency or lack of action.", 
# "Analyze the following text and rate the level of linguistic agency attributed to the target group [woman/women] as either High, Low or Neutral. High agency means the text portrays the target as active agents making decisions and taking actions. Low agency means the targets are portrayed as passive recipients of actions. Neutral means an equal or unclear level of agency. Provide a brief 1-2 sentence justification for your rating referencing specific words or phrases that influenced the agency level." , 
# "Imagine you are a researcher analyzing how language portrays the agency of different groups. Rate the level of linguistic agency given to [women/a woman] in the following text as High, Low or Neutral. \nHigh agency = target is an active decision-maker and doer. \nLow agency = target is a passive recipient of others' actions. \nNeutral = balanced or unclear agency. \nAfter rating, identify 1-2 key words or phrases that support your analysis. Suggest how the text could be rephrased to increase, decrease or maintain the same agency level.", 
# "You will be given a series of text snippets to annotate for the level of linguistic agency attributed to the target group [woman/women]. For each snippet, provide a rating of High, Low or Neutral based on these criteria: \nHigh agency = target is portrayed as active, making their own decisions. \nLow agency = target is portrayed as passive, being acted upon. \nNeutral = balanced or unclear level of agency. \nAfter each rating, provide a brief 1-2 sentence justification citing specific words or phrases from the text that support your rating. Do not introduce any additional context or assumptions beyond what is directly stated in each snippet.", 
# "Analyze the following text for the level of linguistic agency attributed to the target group [woman/women]:\n Provide your analysis in this format: Agency Level: [High/Low/Neutral]. \n Key Evidence: [1-2 key words or phrases from the text supporting the agency level]. \nExplanation: [1 sentence explaining how the key evidence demonstrates the agency level] Consider only the information explicitly stated in the given text. Do not introduce any additional context or assumptions."
# ]

# column = ["classification1", "classification2", "classification3", "classification4", "classification5", "classification6"]

# # %%
# prompt = [
#     '''Given a text snippet, please analyze the description of actions, behaviors, and attributes related to the subject 'woman/women'. Classify the agency as 'High', 'Neutral', or 'Low'. 'High' should be used when the subject takes clear independent actions or makes decisions. 'Low' is for descriptions that present the subject as passive. Use 'Neutral' for descriptions that do not clearly fit either 'High' or 'Low', or when actions and passivity are equally presented.''',
#     '''Examine the narrative in the text to determine the agency of 'women' by breaking down the actions, behaviors, and descriptors. Categorize the agency as 'High' if women are shown with self-governance, 'Neutral' if their agency is ambiguous or mixed, and 'Low' if dependency or lack of action is depicted. Provide your rationale for each determination.
#     ''',
#     '''Review the text and assign a level of linguistic agency to the target group [woman/women] as 'High', 'Low', or 'Neutral'. 'High' agency signifies active roles, 'Low' signifies passive roles, and 'Neutral' is when the agency level is mixed or not clear. Support your rating with a brief explanation citing the text
#     ''',
#     '''Imagine you are an impartial linguistic researcher studying how language portrays the agency of different groups. Assess the portrayal of agency for [women/a woman] in the text. Assign 'High' for active roles, 'Low' for passive roles, and 'Neutral' for balanced or ambiguous portrayals. Justify your rating with key words or phrases from the text and suggest rephrasing to alter the perceived agency level.
#     ''',
#     '''Annotate each text snippet with an agency level for the target group [woman/women] as 'High', 'Low', or 'Neutral', where 'High' is for active portrayal, 'Low' for passive, and 'Neutral' when the portrayal doesn't clearly lean towards active or passive or includes both. Provide a justification for your rating based on the text.
#     ''',
#     '''Analyze the text and indicate the agency level for [woman/women] as 'High', 'Low', or 'Neutral'. Detail the evidence from the text that supports this level and explain the connection. Remember to classify as 'Neutral' when the text presents a balanced or ambiguous portrayal of agency.'''
# ]

# column = ["classification1", "classification2", "classification3", "classification4", "classification5", "classification6", "classification7"]

# # %%
# prompt = [
#     '''Given a text snippet, please analyze the description of actions, behaviors, and attributes related to the subject 'woman/women'. Classify the agency as 'High', 'Neutral', or 'Low'. 'High' should be used when the subject takes clear independent actions or makes decisions. 'Low' is for descriptions that present the subject as passive. Use 'Neutral' for descriptions that do not clearly fit either 'High' or 'Low', or when actions and passivity are equally presented.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',
#     '''Examine the narrative in the text to determine the agency of 'women' by breaking down the actions, behaviors, and descriptors. Categorize the agency as 'High' if women are shown with self-governance, 'Neutral' if their agency is ambiguous or mixed, and 'Low' if dependency or lack of action is depicted. Provide your rationale for each determination.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',
#     '''Review the text and assign a level of linguistic agency to the target group [woman/women] as 'High', 'Low', or 'Neutral'. 'High' agency signifies active roles, 'Low' signifies passive roles, and 'Neutral' is when the agency level is mixed or not clear. Support your rating with a brief explanation citing the text
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',
#     '''Imagine you are an impartial linguistic researcher studying how language portrays the agency of different groups. Assess the portrayal of agency for [women/a woman] in the text. Assign 'High' for active roles, 'Low' for passive roles, and 'Neutral' for balanced or ambiguous portrayals. Justify your rating with key words or phrases from the text and suggest rephrasing to alter the perceived agency level.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',
#     '''Annotate each text snippet with an agency level for the target group [woman/women] as 'High', 'Low', or 'Neutral', where 'High' is for active portrayal, 'Low' for passive, and 'Neutral' when the portrayal doesn't clearly lean towards active or passive or includes both. Provide a justification for your rating based on the text.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',
#     '''Analyze the text and indicate the agency level for [woman/women] as 'High', 'Low', or 'Neutral'. Detail the evidence from the text that supports this level and explain the connection. Remember to classify as 'Neutral' when the text presents a balanced or ambiguous portrayal of agency.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# '''
# ]

# column = ["classification1", "classification2", "classification3", "classification4", "classification5", "classification6"]

# # %%
# prompt = [
#     '''When analyzing the text, break down longer sentences or multiple sentence constructions into distinct thoughts or actions. Then, evaluate each for the level of agency (High, Low, or Neutral) attributed to 'woman/women.' If conflicting signals of agency are present, such as both high and low within the same sentence or connected sentences, annotate the overall agency as 'Neutral.' Additionally, if any part of the text includes 'Neutral' agency, consider the overall agency as 'Neutral.' Ensure to base your analysis on the explicit content without adding context or assumptions.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',

#     '''Dissect the text into separate components if it consists of longer or multiple sentences. Examine each for the agency level: 'High' for actions or attributes suggesting self-governance, 'Low' for indications of passivity or dependency, and 'Neutral' for ambiguous or conflicting agency within the text. Where there are mixed signals, such as high agency and low agency present together, or when neutral agency is suggested, classify the overall agency as 'Neutral.' Follow the text closely, avoiding adding context not provided.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',

#     '''Assess each segment of the text if it contains several sentences or a longer sentence. Rate the agency as 'High,' 'Low,' or 'Neutral.' When the text shows both 'High' and 'Low' agency or includes 'Neutral' agency, assign an overall 'Neutral' agency rating. Support your decision with specific phrases from the text that exemplify these agency levels, and refrain from introducing extra context or assumptions.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',

#     '''Imagine you are an impartial linguistic researcher studying how language portrays the agency of different groups. Dissect the text into parts if it is composed of multiple sentences or is lengthy. Assign an agency rating to each part. In cases where conflicting agency levels are apparent within the parts, or if 'Neutral' agency is detected, consider the entire sentence to express 'Neutral' agency. Identify supporting keywords or phrases, and offer rephrasing suggestions that could shift the agency level.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',

#     '''Segment the text into individual parts if it contains a lengthy sentence or multiple sentences. Evaluate each for 'High,' 'Low,' or 'Neutral' agency. For texts with mixed signals (both 'High' and 'Low') or instances where 'Neutral' agency is present, classify the entire text as having 'Neutral' agency. Provide a brief rationale citing specific evidence from the text and avoid extrapolating beyond the given information.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# ''',

#     '''In your analysis, divide longer sentences or texts with multiple sentences into discrete elements. Determine the agency level for each element as 'High,' 'Low,' or 'Neutral.' When elements within the text present both 'High' and 'Low' agency, or if 'Neutral' is observed, annotate the whole as 'Neutral.' Format your analysis with the agency level, key evidence, and a concise explanation that adheres strictly to the text.
#     "Examples of how to annotate agentic language:
# 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
# Agency: Neutral
# Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
# 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
# Agency: Low
# Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
# 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
# Agency: Neutral
# Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
# 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
# Agency: High
# Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them."
# '''
# ]

# column = ["classification1", "classification2", "classification3", "classification4", "classification5", "classification6"]

# # %%
# prompt = [
#     '''Please assess the level of agency conveyed in the following texts. Agency refers to expressions that indicate an ability or intention to act, influence, or make decisions. For each text, determine whether the agency level is 'High', 'Neutral', or 'Low'. For example, a statement like 'This person has organized the community to act' should be rated 'High' due to the active organization and leadership displayed.''',
#     '''When annotating for agency, consider the text's context and the roles or actions attributed to individuals. Assess whether the text implies active participation, passivity, or neutral involvement. For instance, a text stating 'The person was seen leading the discussions' implies high agency because it attributes a leading role.
#     ''',
#     '''Rate the level of agency in each text using the following scale:

# High Agency: The text explicitly describes decision-making, influencing others, or initiating actions.
# Neutral Agency: The text describes actions or behaviors without clear indicators of personal or direct influence.
# Low Agency: The text portrays individuals as passive or influenced by others without personal control or influence.
# Please apply these criteria to each text, considering the presence and intensity of agency-related words and contexts.''',
#     '''Consider the following statement and analyze its components to determine the level of agency. Start by identifying the subject of the action. Ask yourself: Is the subject performing an action, influencing others, or merely being described? Then, assess the action’s impact: Does the action demonstrate control, influence, or passivity? For instance, if a text says, 'The leader organized a successful rally', the leader (subject) organizes (action) which shows influence and control (impact), indicating High Agency.''',
#     '''Imagine you are reading a report on community actions. In each statement, determine who is taking action and the nature of these actions. Are they shaping their environment, responding to it, or simply existing within it? Rate the agency as 'High' if the individual is proactively shaping circumstances, 'Neutral' if they are simply participating without clear influence, and 'Low' if they are passive or primarily being acted upon. For example, 'She campaigned for cleaner streets and persuaded the council to act' shows proactive shaping and should be rated 'High'.''',
#     '''Start by reading the text superficially to get a general idea. Next, focus on verbs and subjects: What actions are being taken, and who is initiating these actions? Finally, consider the consequences of these actions: Are they affecting others, changing situations, or maintaining a status quo? Use this information to decide if the agency is 'High', 'Neutral', or 'Low'. Example: In 'The committee decided to implement new policies', 'the committee' (subject) 'decided' (verb) affects change (consequence), indicating a 'High' level of agency.'''
# ]

column = ["classification1", "classification2", "classification3", "classification4", "classification5", "classification6", "classification7"]

# %%
prompt = ['''Given a text snippet, please analyze the description of actions, behaviors, and attributes related to the subject 'woman/women'. Classify the agency as 'High', 'Neutral', or 'Low'. 'High' should be used when the subject takes clear independent actions or makes decisions. 'Low' is for descriptions that present the subject as passive or portrayed as passive recipients of actions. Use 'Neutral' for descriptions that do not clearly fit either 'High' or 'Low', or when actions and passivity are equally presented. Provide a brief 1-2 sentence justification for your rating referencing specific words or phrases that influenced the agency level.

Examples of how to annotate agentic language:

<< Example 1) Text: ""what a stupid ugly looking woman. Yet again, all women end up being dead ugly at some point""
Step 1: Identify the phrase and verbs related to woman/women. Here they are “stupid ugly looking woman” and “all women end up being dead ugly”. 
Step 2: Identify the agency level with reasoning of all phrases related to woman/women. Both the phrases comment on womens’ look and does not comment on any particular level of autonomy or action on their part. Thus both give the agency level as “Neutral” and “Neutral”.
Step 3: Calculate the overall agency level based if it is all “High” then “High”, if it is all “Low” then “Low”, if there is any one phrase indicating “Neutral” agency then overall agency level will be “Neutral”. If there are both “High” and “Low” then again it will be “Neutral”. Here all are “Neutral”.
Step 4: Give explanation for the reasoning. 
Explanation: This sentence presents a derogatory and generalizing statement about women's appearance without attributing any actions or decisions to the subject. It reflects the speaker's negative bias and opinion, which does not confer any particular level of autonomy or action upon women. Therefore, it is neutral in terms of agency as it neither explicitly states nor implies women's active participation or passivity in becoming ""dead ugly"".
Step 5: Overall Agency: Neutral >>

<< Example 2) Text: ""you were really weak in writing at school, how have you become a writer, women shouldnt be writers.""
Step 1: Identify the phrase and verbs related to woman/women. Here they are “you were really weak in writing at school” ,”how have you become a writer ”and “women shouldnt be writers”. The first two indirectly refer to woman/women inferring from the third phrase where the speaker attributes the gender of the subject. 
Step 2: Identify the agency level with reasoning of all phrases related to woman/women. All three phrases here attribute passive recipients of actions to women, with the speaker showing pro-activeness in making the decision and not the woman herself. Thus all three phrases show “Low” agency.
Step 3: Calculate the overall agency level based if it is all “High” then “High”, if it is all “Low” then “Low”, if there is any one phrase indicating “Neutral” agency then overall agency level will be “Neutral”. If there are both “High” and “Low” then again it will be “Neutral”. Here all are “Low”
Step 4: Give explanation for the reasoning. 
Explanation: This sentence has low agency language with respect to women, because it implies that women are not capable of becoming writers, and that they are restricted by social norms and expectations.
Step 5: Overall Agency: Low >>

<< Example 3) Text:  ""Julie was smoking, which women shouldn't do, at least get up off the filthy ground, so fucking disgusting to smoke next to a child, dirty fucking tramp""
Step 1: Identify the phrase and verbs related to woman/women. Here they are “Julie was smoking”, “which women shouldn't do” and “dirty fucking tramp”. 
Step 2: Identify the agency level with reasoning of all phrases related to woman/women. The first phrase shows the woman i.e. Julie actively participating in an event i.e. smoking thus agency is “High”. The second phrase shows the speaker making the decision what woman should do making woman a passive recipients of actions. The third phrase is a comment on Julie’s appearance making it “Neutral” agency from the point of view of woman. Thus in all they give the agency level as “High” and “Low” and “Neutral”.
Step 3: Calculate the overall agency level based if it is all “High” then “High”, if it is all “Low” then “Low”, if there is any one phrase indicating “Neutral” agency then overall agency level will be “Neutral”. If there are both “High” and “Low” then again it will be “Neutral”. Overall agency level here is “Neutral”.
Step 4: Give explanation for the reasoning. 
Explanation: The sentence shows mixed agency: Julie's choice to smoke demonstrates high agency because it is an action she is taking independently. However, the speaker's assertion that women shouldn't smoke imposes a judgment and suggests that women should adhere to certain expectations, attributing low agency to women by implying they should follow external guidance. The overall agency is thus ""Neutral"" due to this mix of Julie's individual high agency and the low agency implied for women in general by the speaker's comment.
Step 5: Overall Agency: Neutral >>

<< Example 4) Text: ""They expect from us, men, regardless of our sexual orientation, to bring more and more to the table, while they do nothing at all.""
Step 1: Identify the phrase and verbs related to woman/women. Here they are “They expect from us, men” and “while they do nothing at all”. Both indirectly refer to woman/women inferring from the speaker attributing himself as a man and talking about the other gender. 
Step 2: Identify the agency level with reasoning of all phrases related to woman/women. Both phrases show women proactiveness and the power to make their own decision from the point of view of the speaker. Hence, the agency level is “High” and “High”
Step 3: Calculate the overall agency level based if it is all “High” then “High”, if it is all “Low” then “Low”, if there is any one phrase indicating “Neutral” agency then overall agency level will be “Neutral”. If there are both “High” and “Low” then again it will be “Neutral”. Overall agency will become “High”.
Step 4: Give explanation for the reasoning. 
Explanation: This sentence has high agency language with respect to women, because it implies that women have a lot of control and influence over men, and that they are deliberately exploiting them.
Step 5: Overall Agency: High >>
''']
column = ["classification1"]

# Prompt for baseline classification
# prompt = ['''Analyze the following text and rate the level of agency attributed to the target group [woman/women] as either High, Low or Neutral.          
# ''']
# column = ["classification1"]

# %%
import pandas as pd
from openai import OpenAI
client = OpenAI(api_key="Enter your API key here")

# Ensure the 'text' column exists in your Excel sheet
if 'text' not in df1.columns:
    raise ValueError("The Excel file does not contain a 'text' column.")

#prompt = "Follow a chain of thought for determining the agency of 'women' within the text by breaking down the narrative into actions, behaviors, and descriptors. Then categorize the agency as 'High' where the text endows women with self-governance, 'Neutral' for ambiguous or undetermined agency, and 'Low' for depictions of dependency or lack of action."
for i in range(1):
    # A function that takes a piece of text and returns its classification
    def classify_text(text):
        # Construct the prompt with the definition, examples, and the text to classify
        system_prompt = f"{prompt[i]}" 
        user_prompt = f"Please classify the following text according to the level of linguistic agency:{text} using the 5 step process. Also, please end your response with overall agency level in a new line."
        # Make the API call to OpenAI using the chat endpoint
        response = client.chat.completions.create(
            model="gpt-3.5-turbo",  # Specify the model
            messages=[{"role": "system", "content": system_prompt}, {"role": "user", "content": user_prompt}],  # System message with the prompt
            temperature= 0.0,  # Set to 0 for more deterministic completions
            max_tokens=1024,  # Maximum number of tokens to generate; adjust as needed
            stop=None,  # Sequence where the API should stop generating further tokens
            n=1,  # The number of completions to generate, set to 1 for a single classification
            logprobs=None,  # Include log probabilities; set to None if not needed

        )
        # Extract the text from the response, ensuring we only get the classification part
        classification = response.choices[0].message.content
        return classification.strip("\n")

    # Apply the classification function to each row in the dataframe
    # The result is a new dataframe column with classifications
    df1.loc[:,column[i]] = df1['text'].apply(classify_text)

# Save the updated dataframe with classifications back to an Excel file
# Replace '/path/to/your/updated_Annotations.xlsx' with your desired output file path
# df.to_excel('/path/to/your/updated_Annotations.xlsx', index=False)

# print("Classification complete. The updated Excel file has been saved.")


# %%
# #show the full row and column
# pd.set_option('display.max_columns', None)
# pd.set_option('display.max_rows', None)
# pd.set_option('display.width', None)
# pd.set_option('display.max_colwidth', None)
df1.columns

# %%
#iterate all columns "classification1", "classification2", "classification3", "classification4", "classification5", "classification6" and extract the agency level by checking for the presence of "High", "Low" or "Neutral" in the column and put them in new column "agency_level"
column_agency = ["classification1_agency", "classification2_agency", "classification3_agency", "classification4_agency", "classification5_agency", "classification6_agency"]

for i in range(1):
    df1.loc[:,column_agency[i]] = df1[column[i]].apply(lambda x: 'High' if 'High' in x else ('Low' if 'Low' in x else 'Neutral'))



# %%
#write the above in a for loop for all 7 columns
for i in range(1):
    df1.loc[:, f'accuracy{i+1}'] = df1.apply(lambda x: 1 if x[column_agency[i]] == x['human'] else 0, axis = 1)

# %%
#print the sum of all accuracies
for i in range(1):
    print(df1[f'accuracy{i+1}'].sum())

# %%
df1.human.value_counts()

# %%
#print value counts of all columns classification_agency

for i in range(1):
    print(df1[column_agency[i]].value_counts())





#Below is the code for confusion matrix creation
# %%
import pandas as pd
df1 = pd.read_csv(r'C:\Users\adity\OneDrive - Students RWTH Aachen University\Master Thesis\Dehumanization\Correlation\df_final_llama3_8b.csv',encoding='cp1252')

# %%
import pandas as pd
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt

# Ratings given by bertagent and human

LLM_ratings = df1.classification1_agency.to_list()
human_ratings = df1.human.to_list()
# Mapping from text ratings to numerical values
mapping = {'Low': 0, 'Neutral': 1, 'High': 2}

# Encode the text ratings as numerical values
encoded_LLM = [mapping[rating] for rating in LLM_ratings]
encoded_human = [mapping[rating] for rating in human_ratings]

# Generate the confusion matrix
cm = confusion_matrix(encoded_LLM, encoded_human)

# Create a pandas dataframe to display the confusion matrix in a table format
cm_df = pd.DataFrame(cm, 
                     index=['Low', 'Neutral', 'High'], 
                     columns=['Low', 'Neutral', 'High'])

# Plot the confusion matrix using seaborn
plt.figure(figsize=(10, 7))
sns.heatmap(cm_df, annot=True, cmap='Blues', fmt='g')
plt.title('Confusion Matrix')
plt.ylabel('LLM Ratings')
plt.xlabel('Human Ratings')
plt.show()
