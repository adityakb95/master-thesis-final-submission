# %%
import pandas as pd

df1 = pd.read_csv("Enter path of csv file")

# %%
!pip install spacy-experimental
!pip install https://github.com/explosion/spacy-experimental/releases/download/v0.6.0/en_coreference_web_trf-3.4.0a0-py3-none-any.whl#egg=en_coreference_web_trf
!python -m spacy download en_core_web_sm
! git clone https://github.com/maartensap/riveter-nlp.git

# %%
import sys
sys.path.insert(0, 'Enter path of riveter-nlp/riveter')

from collections import defaultdict
import os
import pandas as pd
import random
import spacy_experimental
import spacy
from riveter import Riveter

import seaborn as sns
import matplotlib.pyplot as plt

# %%
riveter = Riveter()
riveter.load_sap_lexicon('agency')

# %%
df1['ind'] = range(1, len(df1) + 1)

# %%
riveter.train(df1.text.tolist(), df1.ind.tolist())

# %%
#df1['score1'] = df1['ind'].apply(lambda x: riveter.get_scores_for_doc(x))
df1['score1'] = [riveter.get_scores_for_doc(x) for x in df1['ind']]

#create a function that takes the score and returns the sum of all the values of every key, if the dictionary is empty return 0. Store it the sum in another column called score_total
def sum_score(score):
    if score == {}:
        return 0
    else:
        return sum(score.values())
    
df1['score_total1'] = df1['score1'].apply(sum_score)


